package goquick

import (
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
)

type CodeGenerator interface {
	Generate(RenderData) (string, error)
	PathName() string
	FileName() string
}

type ProtoGenerator interface {
	GenerateAll([]Service, *plugin.CodeGeneratorResponse) error
}

type PathGenerator interface {
	PackagePath(pathName string, protoPackage string) string
	FileRelativePath(pathName string, protoPackage string, fileName string, serviceName string) string
}

type Template struct {
	FileName string
	PathName string
	Template string
}

type Method struct {
	Name       string
	InputType  string
	OutputType string
}

type Service struct {
	Name         string
	Methods      []Method
	ProtoPackage string
}

type RenderData struct {
	Service       Service
	PathGenerator PathGenerator
}
