package protogenerator

import (
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

type protoGenerator struct {
	codeGenerator goquick.CodeGenerator
	pathGenerator goquick.PathGenerator
}

func New(codeGenerator goquick.CodeGenerator, pathGenerator goquick.PathGenerator) goquick.ProtoGenerator {
	return protoGenerator{
		codeGenerator: codeGenerator,
		pathGenerator: pathGenerator,
	}
}

func (p protoGenerator) GenerateAll(services []goquick.Service, codeGeneratorResponse *plugin.CodeGeneratorResponse) error {
	pathName := p.codeGenerator.PathName()
	fileName := p.codeGenerator.FileName()

	for _, service := range services {
		fileName := p.pathGenerator.FileRelativePath(pathName, service.ProtoPackage, fileName, service.Name)

		renderData := goquick.RenderData{
			Service:       service,
			PathGenerator: p.pathGenerator,
		}

		fileContent, err := p.codeGenerator.Generate(renderData)

		if err != nil {
			return err
		}

		file := &plugin.CodeGeneratorResponse_File{
			Content: &fileContent,
			Name:    &fileName,
		}

		codeGeneratorResponse.File = append(codeGeneratorResponse.File, file)
	}
	return nil
}
