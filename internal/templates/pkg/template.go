package pkg

import (
	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

var Template = goquick.Template{
	PathName: "pkg",
	FileName: "pkg",
	Template: `
package servicepkg

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	pb "{{.PathGenerator.PackagePath "root" .Service.ProtoPackage}}"
)

type Service interface {
	{{range .Service.Methods}}
	{{.Name}}(context.Context, pb.{{.InputType}}) (pb.{{.OutputType}}, error)
	{{end}}
}

type Endpoints struct {
	{{range .Service.Methods}}
	{{.Name}} endpoint.Endpoint
	{{end}}
}

{{range .Service.Methods}}
type {{.Name}}Response interface {
	Response() pb.{{.OutputType}}
}
{{end}}
`,
}
