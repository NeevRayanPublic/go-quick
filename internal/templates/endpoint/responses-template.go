package endpoint

import (
	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

var ResponseTemplate = goquick.Template{
	PathName: "endpoint",
	FileName: "response",
	Template: `
package endpoint

import (
	pb "{{.PathGenerator.PackagePath "root" .Service.ProtoPackage}}"
)


{{range .Service.Methods}}
type {{.Name}}Response struct {
	response pb.{{.OutputType}}
	err 	 error
}

func (r {{.Name}}Response) Response() pb.{{.OutputType}} {
	return r.response
}

func (r {{.Name}}Response) Failed() error {
	return r.err
}
{{end}}
`,
}
