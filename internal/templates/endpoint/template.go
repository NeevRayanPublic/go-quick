package endpoint

import (
	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

var EndpointTemplate = goquick.Template{
	PathName: "endpoint",
	FileName: "endpoint",
	Template: `
package endpoint

import (
	"context"

	pkg "{{.PathGenerator.PackagePath "pkg" .Service.ProtoPackage}}"
	pb "{{.PathGenerator.PackagePath "root" .Service.ProtoPackage}}"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/NeevRayanPublic/gotools/rich-error"
	"gitlab.com/NeevRayanPublic/gotools/middleware"
)

func MakeEndpoints(middlewares []endpoint.Middleware, svc pkg.Service) pkg.Endpoints {
	{{range .Service.Methods}}
	{{.Name}}Endpoint := func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(*pb.{{.InputType}})
		if !ok {
			return nil, richerror.New("Failed to cast requset!")
		}

		res, err := svc.{{.Name}}(ctx, *req)

		return {{.Name}}Response{
			response: res,
			err: err,
		}, nil
	}
	{{end}}

	return pkg.Endpoints{
		{{range .Service.Methods}}
		{{.Name}}:     middleware.ApplyMiddlewares({{.Name}}Endpoint, middlewares),
		{{end}}
	}
}
`,
}
