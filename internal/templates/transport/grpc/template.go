package transportgrpc

import (
	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

var Template = goquick.Template{
	PathName: "transport-grpc",
	FileName: "grpctransport",
	Template: `
package grpctransport

import (
	"context"
	"errors"

	kitgrpc "github.com/go-kit/kit/transport/grpc"
	pkg "{{.PathGenerator.PackagePath "pkg" .Service.ProtoPackage}}"
	pb "{{.PathGenerator.PackagePath "root" .Service.ProtoPackage}}"
	"gitlab.com/NeevRayanPublic/gotools/failure"
)

type grpcServer struct {
	{{range .Service.Methods}}
	_{{.Name}} kitgrpc.Handler
	{{end}}
}

func NewGRPCServer(endpoints pkg.Endpoints) pb.{{.Service.Name}}Server {
	options := []kitgrpc.ServerOption{
		// kitgrpc.ServerErrorLogger(logger),
	}

	return &grpcServer{
		{{range .Service.Methods}}
		_{{.Name}}: kitgrpc.NewServer(
			endpoints.{{.Name}},
			decode{{.Name}}GRPCRequest,
			encode{{.Name}}GRPCResponse,
			options...,
		),
		{{end}}
	}
}

{{range .Service.Methods}}
func (s *grpcServer) {{.Name}}(ctx context.Context, req *pb.{{.InputType}}) (*pb.{{.OutputType}}, error) {
	_, resp, err := s._{{.Name}}.ServeGRPC(ctx, req)

	if err != nil {
		return nil, err
	}

	return resp.(*pb.{{.OutputType}}), nil
}

func decode{{.Name}}GRPCRequest(_ context.Context, request interface{}) (interface{}, error) {
	return request, nil
}

func encode{{.Name}}GRPCResponse(_ context.Context, response interface{}) (interface{}, error) {
	if resp, ok := response.(failure.Failure); ok && resp.Failed() != nil {
		return nil, resp.Failed()
	}

	resp, ok := response.(pkg.{{.Name}}Response)

	if !ok {
		return nil, errors.New("Faield to cast response to payment.Response (programmer error)")
	}

	result := resp.Response()
	return &result, nil
}
{{end}}
`,
}
