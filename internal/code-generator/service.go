package codegenerator

import (
	"html/template"
	"strings"

	goquick "gitlab.com/NeevRayanPublic/go-quick/pkg"
)

type codeGenerator struct {
	tmpl     *template.Template
	pathName string
	fileName string
}

func New(goQuickTemplate goquick.Template) goquick.CodeGenerator {
	tmpl, err := template.New("Template").Parse(goQuickTemplate.Template)

	if err != nil {
		panic(err)
	}

	return codeGenerator{
		tmpl:     tmpl,
		pathName: goQuickTemplate.PathName,
		fileName: goQuickTemplate.FileName,
	}
}

func (g codeGenerator) Generate(renderData goquick.RenderData) (string, error) {
	buf := &strings.Builder{}
	err := g.tmpl.Execute(buf, renderData)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (g codeGenerator) PathName() string {
	return g.pathName
}

func (g codeGenerator) FileName() string {
	return g.fileName
}
