package pathgenerator

import (
	"fmt"

	"gitlab.com/NeevRayanPublic/go-quick/pkg"
)

type pathGenerator struct {
	goPackageRoot string
}

func New(goPackageRoot string) goquick.PathGenerator {
	return pathGenerator{
		goPackageRoot: goPackageRoot,
	}
}

func (p pathGenerator) PackagePath(pathName string, protoPackage string) string {
	panicIfPathNameDoesNotExist(pathName)
	if reletivePath[pathName] == "" {
		return fmt.Sprintf("%s/%s", p.goPackageRoot, protoPackage)
	}
	return fmt.Sprintf("%s/%s/%s", p.goPackageRoot, protoPackage, reletivePath[pathName])
}

func (p pathGenerator) FileRelativePath(pathName string, protoPackage string, fileName string, serviceName string) string {
	panicIfPathNameDoesNotExist(pathName)
	return fmt.Sprintf("%s/%s/%s_%s.go", protoPackage, reletivePath[pathName], fileName, serviceName)
}

func panicIfPathNameDoesNotExist(pathName string) {
	if _, ok := reletivePath[pathName]; !ok {
		panic(fmt.Sprintf("%s does not exit as a path name", pathName))
	}
}

var reletivePath = map[string]string{
	"root":           "",
	"pkg":            "pkg",
	"transport-grpc": "transport/grpc",
	"transport-rest": "transport/rest",
	"endpoint":       "endpoint",
}
