package main

import (
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	goquick "gitlab.com/NeevRayanPublic/go-quick/pkg"
)

func getServices(req *plugin.CodeGeneratorRequest, goPkgRoot string) []goquick.Service {
	services := []goquick.Service{}

	for _, protoFile := range req.ProtoFile {
		if !contains(req.FileToGenerate, *protoFile.Name) {
			continue
		}

		protoPackageName := *protoFile.Package

		for _, protoService := range protoFile.Service {
			service := goquick.Service{
				Name:         *protoService.Name,
				ProtoPackage: protoPackageName,
			}

			methods := []goquick.Method{}

			for _, protoMethod := range protoService.Method {
				method := goquick.Method{
					Name:       *protoMethod.Name,
					InputType:  getDataType(*protoMethod.InputType, protoPackageName),
					OutputType: getDataType(*protoMethod.OutputType, protoPackageName),
				}
				methods = append(methods, method)
			}

			service.Methods = methods
			services = append(services, service)
		}
	}

	return services
}

func getDataType(protoType string, protoPackage string) string {
	if protoType[0:1] == "." {
		prefix_len := len(protoPackage) + 2 // .package.
		return protoType[prefix_len:]
	}
	panic("I don't know what to do with this type!")
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
