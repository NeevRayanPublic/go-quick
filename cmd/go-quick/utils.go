package main

import (
	"log"
)

func panicIfError(err error, message string) {
	if err != nil {
		log.Fatalf("%s -> %s", message, err.Error())
	}
}
