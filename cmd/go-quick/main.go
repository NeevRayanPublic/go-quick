package main

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/NeevRayanPublic/go-quick/internal/code-generator"

	"gitlab.com/NeevRayanPublic/go-quick/internal/path-generator"
	"gitlab.com/NeevRayanPublic/go-quick/internal/templates/endpoint"

	"gitlab.com/NeevRayanPublic/go-quick/pkg"

	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"

	"github.com/golang/protobuf/proto"

	"gitlab.com/NeevRayanPublic/go-quick/internal/proto-generator"
	"gitlab.com/NeevRayanPublic/go-quick/internal/templates/pkg"
	"gitlab.com/NeevRayanPublic/go-quick/internal/templates/transport/grpc"
)

var (
	goPkgRoot = "gitlab.com/NeevRayan/timche-interface"
)

func main() {
	data, readErr := ioutil.ReadAll(os.Stdin)
	panicIfError(readErr, "Failed to read from stdin")

	codeGeneratorRequest := &plugin.CodeGeneratorRequest{}
	unmarshalErr := proto.Unmarshal(data, codeGeneratorRequest)
	panicIfError(unmarshalErr, "Failed to unmarshal codeGeneratorRequest")

	if codeGeneratorRequest.Parameter != nil {
		goPkgRoot = *codeGeneratorRequest.Parameter
	}

	pathGenerator := pathgenerator.New(goPkgRoot)

	codeGeneratorResponse := &plugin.CodeGeneratorResponse{}

	services := getServices(codeGeneratorRequest, goPkgRoot)

	for _, template := range templates {
		codeGenerator := codegenerator.New(template)
		protoGenerator := protogenerator.New(codeGenerator, pathGenerator)
		err := protoGenerator.GenerateAll(services, codeGeneratorResponse)
		panicIfError(err, fmt.Sprintf("Failed to generate '%s'", template.FileName))
	}

	output, marshalErr := proto.Marshal(codeGeneratorResponse)
	panicIfError(marshalErr, "Failed to marshal codeGeneratorResponse")

	writeErr := binary.Write(os.Stdout, binary.LittleEndian, output)
	panicIfError(writeErr, "Failed to write data to stdout")
}

var templates = []goquick.Template{
	transportgrpc.Template,
	pkg.Template,
	endpoint.ResponseTemplate,
	endpoint.EndpointTemplate,
}
