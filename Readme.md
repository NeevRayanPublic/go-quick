# Go Quick

go-quick is a project that helps you create a set of go based microservices by creating a bunch of (boiler plate like) 
codes from your grpc defenitions.

go-quick creates your basic interfaces, along side with your endpoint and transport layers.
It uses go-kit library for endpoint and transport layer.
It also uses protobuf messages as Data Transport Objects so it passes your request object to your service and takes
a response object.

Currently it's working in a very restricted way. Most its important restriction is that you have to put your request/response messages in the same package as the service.
